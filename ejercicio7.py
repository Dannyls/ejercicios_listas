#___Autor:"Danny Lima"____
#___Email:danny.lima@unl.edu.ec

# Dada una lista de números, intercambie elementos adyacentes
# en cada par (intercambie A [0] con A [1] , A [2] con A [3] ,
# etc.). Imprime la lista resultante. Si una lista tiene un
# número impar de elementos, deje el último elemento intacto.
# Lea una lista de enteros:
a = [int(s) for s in input().split()]
# Print a value:
# print(a)
for s in range(1, len(a), 2):
    a[s-1], a[s] = a[s], a[s-1]
print(' '.join([str(s) for s in a]))
