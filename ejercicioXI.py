#___Autor:"Danny Lima"____
#___Email:danny.lima@unl.edu.ec

# Dada una lista de números, busque e imprima los elementos
# que aparecen en ella solo una vez. Dichos elementos deben
# imprimirse en el orden en que aparecen en la lista original.
# Read a list of integers:
a = [int(s) for s in input().split()]
# Print a value:
# print(a)
for s in a:
    if a.count(s) == 1:
        print(s, end=" ")
