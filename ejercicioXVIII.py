#___Autor:"Danny Lima"____
#___Email:danny.lima@unl.edu.ec

# Dado un entero positivo impar n, producir una matriz bidimensional de
# tamaño nxn. Rellene cada elemento con el carácter ".". Luego llene la
# fila central, la columna central y las diagonales con el carácter "*".
# Obtendrás una imagen de un copo de nieve. Imprima el copo de nieve en nxn
# filas y columnas y separe los caracteres con un solo espacio.
# Read an integer:
n = int(input())
# Print a value:
# print(a)
a = [["."]*n for i in range(n)]
for y in range(n):
    for x in range(n):
        if x == y:
            a[y][x] = "*"
        elif x == (n-1)/2:
            a[y][x] = "*"
        elif x + y == 6:
            a[y][x] = "*"
        elif y == (n-1)/2:
            a[y][x] = "*"
for linea in a:
    print(' '.join([str(elem) for elem in linea]))
