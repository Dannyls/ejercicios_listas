#___Autor:"Danny Lima"____
#___Email:danny.lima@unl.edu.ec

# Read a list of integers:
# a = [int(s) for s in input().split()]
# Print a value:
# print(a)
x = []
y = []
for i in range(8):
    a = [int(s) for s in input().split()]
    x.append(a[0])
    y.append(a[1])

result = True
for i in range(8):
    for j in range(i + 1, 8):
        if x[i] == x[j] or y[i] == y[j] or abs(x[i] - x[j]) == abs(y[i] - y[j]):
            result = False

if result:
    print("NO")
else:
    print("YES")
