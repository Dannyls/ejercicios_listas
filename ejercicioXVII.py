#___Autor:"Danny Lima"____
#___Email:danny.lima@unl.edu.ec

# Dados dos enteros (el número de filas m y las columnas n
# de la mxn 2d lista) y las siguientes m filas de n enteros,
# seguidas por dos enteros no negativos i y j menores que n,
# intercambie las columnas i y j de la lista 2d e imprimen
# el resultado.
# Read a 2D list of integers:
m, n = [int(j) for j in input().split()]
a = [[int(j) for j in input().split()] for i in range(m)]
# Print a value:
# print(a)
columna1, columna2 = [int(j) for j in input().split()]
for i in range(m):
    a[i][columna1], a[i][columna2] = a[i][columna2], a[i][columna1]
for linea in a:
    print(' '.join([str(elem) for elem in linea]))
