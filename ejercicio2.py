#___Autor:"Danny Lima"____
#___Email:danny.lima@unl.edu.ec

# Dada una lista de números, imprima todos sus elementos
# pares. Use un bucle for que itera sobre la lista misma
# y no sobre sus índices. Es decir, no use range ().
# Lea una lista de enteros:
a = [int(s) for s in input().split()]
# Print a value:
# print(a)
for i in a:
    if i % 2 == 0:
        print(i)
