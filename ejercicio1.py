#___Autor:"Danny Lima"____
#___Email:danny.lima@unl.edu.ec

# Dada una lista de números, encuentre e imprima
# todos sus elementos con índices pares (es decir,
# A [0] , A [2] , A [4] , ...).
# Lea una lista de enteros:
a = [int(s) for s in input().split()]
# print a value:
# print(a)
indice = 0
for i in a:
    if indice % 2 == 0:
        print(i)
    indice = indice+1
