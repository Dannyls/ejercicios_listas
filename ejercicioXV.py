#___Autor:"Danny Lima"____
#___Email:danny.lima@unl.edu.ec

# Dado un entero    n, cree unamatriz bidimensional de tamaño
# nxn  de acuerdo con las  siguientes reglas e imprímala:
# En el antidiagonal poner 1.
# En las diagonales por encima de él poner 0.
# En las diagonales por debajo de él poner 2.
# Read an integer:
n = int(input())
# Print a value:
# print(a)
a = [[0] * n for i in range(n)]
for i in range(n):
    for j in range(n):
        if i + j + 1 < n:
            a[i][j] = 0
        elif i + j + 1 == n:
            a[i][j] = 1
        else:
            a[i][j] = 2
for linea in a:
    print(' '.join([str(elem) for elem in linea]))
