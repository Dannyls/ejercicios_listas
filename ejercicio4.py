#___Autor:"Danny Lima"____
#___Email:danny.lima@unl.edu.ec

# Dada una lista de enteros distintos de cero, encuentre e imprima
# el primer par de elementos adyacentes que tengan el mismo signo.
# Si no existe ese par, imprima 0 .
# Lea una lista de enteros:
a = [int(s) for s in input().split()]
# Print a value:
# print(a)
cont = 1
for s in range(1, len(a)):
    if a[s-1] * a[s] > 0:
        print(a[s-1], a[s])
        break
    else:
        cont = cont+1
        if cont == len(a):
            print("0")
