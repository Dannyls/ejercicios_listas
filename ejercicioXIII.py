#___Autor:"Danny Lima"____
#___Email:danny.lima@unl.edu.ec

# Read a list of integers:
N, K= [int(s) for s in input().split()]
# Print a value:
# print(a)
game = ["I"]*N
for s in range(K):
    left, right = [int(s) for s in input().split()]
    for i in range(left-1, right):
        game[i] = '.'
print(''.join(game))
