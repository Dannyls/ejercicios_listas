#___Autor:"Danny Lima"____
#___Email:danny.lima@unl.edu.ec

# Dada una lista de enteros, encuentre el primer elemento máximo
# en él. Imprime su valor y su índice (contando con 0).
# Lea una lista de enteros:
a = [int(s) for s in input().split()]
# Print a value:
# print(a)
maximo = max(a)
print(maximo, a.index(maximo))
