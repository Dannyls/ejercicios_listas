#___Autor:"Danny Lima"____
#___Email:danny.lima@unl.edu.ec

# Read a list of integers:
n, m = [int(s) for s in input().split()]
# Print a value:
# print(a)
a = [["."]*m for i in range(n)]
for i in range(n):
    for j in range(m):
        if j % 2 != 0 and i % 2 == 0:
            a[i][j] = '*'
        elif j % 2 == 0 and i % 2 != 0:
            a[i][j] = '*'
for linea in a:
    print(' '.join([str(elem) for elem in linea]))
