#___Autor:"Danny Lima"____
#___Email:danny.lima@unl.edu.ec

# Dada una lista de números, encuentre e imprima todos sus
# elementos que sean mayores que su vecino izquierdo.
# Lea una lista de enteros:
a = [int(s) for s in input().split()]
# Print a value:
# print(a)
for s in range(1, len(a)):
    if a[s] > a[s-1]:
        print(a[s])
