#___Autor:"Danny Lima"____
#___Email:danny.lima@unl.edu.ec

# Dada una lista de números distintos, intercambie el
# mínimo y el máximo e imprima la lista resultante.
# Read a list of integers:
a = [int(s) for s in input().split()]
# Print a value:
# print(a)
maximo = a.index(max(a))
minimo = a.index(min(a))
a[minimo], a[maximo] = a[maximo], a[minimo]
print(*a)

