#___Autor:"Danny Lima"____
#___Email:danny.lima@unl.edu.ec

# Dada una lista de números, cuente varios pares de elementos
# iguales. Cualquier dos elementos que sean iguales entre sí
# deben contarse exactamente una vez.
# Lea una lista de enteros:
a = [int(s) for s in input().split()]
# Print a value:
# print(a)
cont = 0
for s in range(len(a)):
    for i in range(s+1, len(a)):
        if a[s] == a[i]:
            cont = cont+1
print(cont)
