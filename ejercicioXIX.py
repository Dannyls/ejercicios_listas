#___Autor:"Danny Lima"____
#___Email:danny.lima@unl.edu.ec

# Dado un número entero n, cree una matriz bidimensional de
# tamaño nxn de acuerdo con las siguientes reglas e impríma:
# En la diagonal principal poner 0.
# En las diagonales adyacentes a la principal poner 1.
# En las siguientes diagonales adyacentes poner 2, y
# así sucesivamente
# Read an integer:
n = int(input())
# Print a value:
# print(a)
b = [[0] * n for i in range(n)]
for i in range(n):
    for j in range(n):
        b[i][j] = abs(i-j)
for linea in b:
    print(' '.join([str(elem) for elem in linea]))
