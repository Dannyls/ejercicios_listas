#___Autor:"Danny Lima"____
#___Email:danny.lima@unl.edu.ec

# Dados dos enteros, el número de filas my columnas
# n de m × n 2d lista, y las siguientes m filas de n
# enteros, seguidos de un entero c . Multiplique cada
# elemento por c e imprima el resultado.
# Print a value:
# print(a)
d = input().split()
NUM_ROWS = int(d[0])
NUM_COLS = int(d[1])
a = [[int(j) for j in input().split()] for i in range(NUM_ROWS)]
c = int(input())
for ci in range(NUM_ROWS):
    for cj in range(NUM_COLS):
        a[ci][cj] = a[ci][cj] * c

for fila in a:
    for columna in fila:
        print(columna, end=" ")
    print()
