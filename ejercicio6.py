#___Autor:"Danny Lima"____
#___Email:danny.lima@unl.edu.ec

# Dada una lista de números con todos los elementos
# ordenados en orden ascendente, determine e imprima
# el número de elementos distintos que contiene.
# Lea una lista de enteros:
a = [int(s) for s in input().split()]
# Print a value:
# print(a)
cont = 1
for s in range(1, len(a)):
    if a[s] != a[s-1]:
        cont = cont + 1
print(cont)
